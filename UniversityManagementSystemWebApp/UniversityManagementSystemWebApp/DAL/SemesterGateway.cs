﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.DAL
{
    public class SemesterGateway:BaseGateway
    {
        public List<Semester> GetAllSemesters()
        {
            List<Semester> semesters = new List<Semester>();
            string query = "SELECT * FROM Semester ORDER BY Id ASC;";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Semester semester = new Semester();
                semester.Id = Convert.ToInt32(Reader["Id"]);
                semester.Name = Reader["Name"].ToString();
                semesters.Add(semester);
            }
            Connection.Close();
            return semesters;

        }
    }
}