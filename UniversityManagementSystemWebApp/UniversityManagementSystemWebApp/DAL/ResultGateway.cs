﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class ResultGateway:BaseGateway
    {
        public int Save(Result result)
        {
            string query = "INSERT INTO Result (StudentId,CourseId,Grade,IsResult) VALUES('" + result.StudentId + "','" + result.CourseId + "','" + result.Grade + "','" + result.IsResult + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public Result CheckCourseResult(int studentId, int courseId)
        {
            string query = "SELECT * FROM Result WHERE CourseID=" + courseId + "AND StudentId=" + studentId + "AND IsResult=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Result result = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                result = new Result();;
                result.Id = Convert.ToInt32(Reader["Id"]);
                result.StudentId = Convert.ToInt32(Reader["StudentId"]);
                result.CourseId = Convert.ToInt32(Reader["CourseId"]);
                result.Grade = Reader["Grade"].ToString();
            }
            Connection.Close();
            return result;
        }
        public int UpdateCourseResult(Result result)
        {
            string query = "UPDATE Result set Grade='" + result.Grade + "'WHERE Id=" + result.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        //public Result SelectedStudentResultViews(int studentId,int courseId)  
        //{
        //   ResultShowView enrollCourseViews = new ResultShowView();
        //   string query = "SELECT * FROM ResultShowView WHERE StudentId=" + studentId + "AND CourseId=" + courseId + "AND IsResult=" + 0;
        //    Command = new SqlCommand(query, Connection);
        //    Connection.Open();
        //    Reader = Command.ExecuteReader();
        //    while (Reader.Read())
        //    {
        //        ResultShowView enrollCourseView = new ResultShowView();
        //        enrollCourseView.Id = Convert.ToInt32(Reader["Id"]);
        //        enrollCourseView.StudentId = Convert.ToInt32(Reader["StudentId"]);
        //        enrollCourseView.CourseCode = Reader["CourseCode"].ToString();
        //        enrollCourseView.CourseName = Reader["CourseName"].ToString();
        //        enrollCourseView.CourseGrade = Reader["CourseGrade"].ToString();
        //        enrollCourseView.CourseCredit = Reader["CourseCredit"].ToString();
        //        enrollCourseViews.Add(enrollCourseView);
        //    }
        //    Connection.Close();
        //    return enrollCourseViews;
        //}

        //start  for unassign the IsResult Courese
        public List<Result> UnassignResultCourses()
        {   
            List<Result> results = new List<Result>();
            string query = "SELECT * FROM Result WHERE IsResult=0";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Result result = new Result();
                result.Id = Convert.ToInt32(Reader["Id"]);
                results.Add(result);
            }
            Connection.Close();
            return results;
        }
        public int UnassignRequest(Result result)
        {
            string query = "UPDATE Result set IsResult= " + result.IsResult + " WHERE Id=" + result.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //start  for unassign the IsResult 
    }
}