﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.DAL
{
    public class DepartmentGateway:BaseGateway
    {
        public int Save(Department department)
        {
            string query = "INSERT INTO Department (Code,Name) VALUES('"+department.Code+ "','" + department.Name + "')";
            Command = new SqlCommand(query,Connection);
            Connection.Open();

            int rowAffect = Command.ExecuteNonQuery();

            Connection.Close();

            return rowAffect;
        }

        public bool IsDepartmentCodeExists(string code) 
        {
            string query = "SELECT * FROM Department WHERE Code='" + code + "' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isDepartmentCode = Reader.HasRows; 
            Connection.Close();
            return isDepartmentCode;
        }

        public bool IsDepartmentNameExists(string name)
        {
            string query = "SELECT * FROM Department WHERE Name='"+name+"' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isDepartmentName = Reader.HasRows; 
            Connection.Close();
            return isDepartmentName;
        }


        public List<Department> ShowAllDepartments()
        {
            List<Department> departments = new List<Department>();
            string query = "SELECT * FROM Department ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Department department = new Department();
                department.Id = Convert.ToInt32(Reader["Id"]);
                department.Code = Reader["Code"].ToString();
                department.Name = Reader["Name"].ToString();
                departments.Add(department);
            }
            Connection.Close();
            return departments;

        }

        public Department GetDepartmentInfo(int id)
        {
            string query = "SELECT * FROM Department WHERE Id=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Department department = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                department = new Department();
                department.Id = Convert.ToInt32(Reader["Id"]);
                department.Code = Reader["Code"].ToString();
                department.Name = Reader["Name"].ToString();
            }
            Connection.Close();
            return department;
        }
    }
}