﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class EnrollCourseGateway:BaseGateway
    {
        public int Save(EnrollCourse enrollCourse)
        {
            string query = "INSERT INTO EnrollCourse (StudentId,CourseId,Date,IsEnroll) VALUES('" + enrollCourse.StudentId + "','" + enrollCourse.CourseId + "','" + enrollCourse.Date + "','" + enrollCourse.IsEnroll + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public EnrollCourse CheckEnrollCourses(int studentId,int courseId)    
        {

            string query = "SELECT * FROM EnrollCourse WHERE CourseID=" + courseId + "AND StudentId=" + studentId + "AND IsEnroll=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            EnrollCourse enrollCourse = null;   
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                enrollCourse = new EnrollCourse();
                enrollCourse.StudentId = Convert.ToInt32(Reader["StudentId"]);
                enrollCourse.CourseId = Convert.ToInt32(Reader["CourseId"]);
            }
            Connection.Close();
            return enrollCourse;
        }

        public List<EnrollCourseView> GetEnrollCourseViews(int id)    
        {
            List<EnrollCourseView> enrollCourseViews = new List<EnrollCourseView>();
            string query = "SELECT * FROM EnrollCourseView WHERE StudentId=" + id + "AND IsEnroll=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                EnrollCourseView enrollCourseView = new EnrollCourseView();
                //enrollCourseView.EnrollId = Convert.ToInt32(Reader["EnrollId"]);
                enrollCourseView.StudentId =(int) Reader["StudentId"];
                enrollCourseView.CourseId = (int) Reader["CourseId"];
                enrollCourseView.CourseName = Reader["CourseName"].ToString();
                enrollCourseView.CourseCode = Reader["CourseCode"].ToString();
                enrollCourseView.CourseCredit = Convert.ToDouble(Reader["CourseCredit"]);
                enrollCourseViews.Add(enrollCourseView);
            }
            Connection.Close();
            return enrollCourseViews;
        }

        //start  for unassign the Enroll Courese
        public List<EnrollCourse> UnassignEnrollCourses()   
        {
            List<EnrollCourse> enrollCourses = new List<EnrollCourse>();
            string query = "SELECT * FROM EnrollCourse WHERE IsEnroll=0";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                EnrollCourse enrollCourse = new EnrollCourse();
                enrollCourse.Id = Convert.ToInt32(Reader["Id"]);
                enrollCourses.Add(enrollCourse);
            }
            Connection.Close();
            return enrollCourses;
        }   
        public int UnassignRequest(EnrollCourse enrollCourse)  
        {
            string query = "UPDATE EnrollCourse set IsEnroll= " + enrollCourse.IsEnroll + " WHERE Id=" + enrollCourse.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //start  for unassign the Enroll Courese
    }
}