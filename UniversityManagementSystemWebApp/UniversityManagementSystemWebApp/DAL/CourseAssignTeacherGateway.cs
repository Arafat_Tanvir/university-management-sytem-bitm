﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class CourseAssignTeacherGateway:BaseGateway
    {
        //CourseAssignTeacher Save
        public int Save(CourseAssignTeacher courseAssignTeacher)
        {
            string query = "INSERT INTO CourseAssignTeacher (DepartmentId,TeacherId,CourseId,IsAssign) VALUES('" + courseAssignTeacher.DepartmentId + "','" + courseAssignTeacher.TeacherId + "','" + courseAssignTeacher.CourseId + "','" + courseAssignTeacher.IsAssign + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //==============LoadCourseDetailsByCourseId==================
        public CourseAssignTeacherView CourseStaticses(int id) 
        {
            string query = "SELECT * FROM CourseAssignTeacherView WHERE CourseId=" + id + " AND IsAssign=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            CourseAssignTeacherView courseAssignTeacher = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                courseAssignTeacher = new CourseAssignTeacherView();
                courseAssignTeacher.TeacherName = Reader["TeacherName"].ToString();
            }
            Connection.Close();
            return courseAssignTeacher;

        }   
        // ===============Checked this course is taken by another Teacher=================?
        public CourseAssignTeacher CourseIsAssignByAnotherTeacher(int id)
        {

            string query = "SELECT * FROM CourseAssignTeacher WHERE CourseId=" + id + " AND IsAssign=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            CourseAssignTeacher courseAssignTeacher = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                courseAssignTeacher = new CourseAssignTeacher();
                courseAssignTeacher.Id = Convert.ToInt32(Reader["Id"]);
                courseAssignTeacher.DepartmentId = Convert.ToInt32(Reader["DepartmentId"]);
                courseAssignTeacher.CourseId = Convert.ToInt32(Reader["CourseId"]);
                courseAssignTeacher.TeacherId = Convert.ToInt32(Reader["TeacherId"]);
            }
            Connection.Close();
            return courseAssignTeacher;
        }
        //==============LoadCourseAssignTeacherByTeacherId==================
        public List<CourseAssignTeacherView> CheckTeacherCredit(int id)  
        {
            List<CourseAssignTeacherView> courseAssignTeacherViews = new List<CourseAssignTeacherView>();
            string query = "SELECT * FROM CourseAssignTeacherView WHERE TeacherId=" + id + " AND IsAssign=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                CourseAssignTeacherView courseAssignTeacherView = new CourseAssignTeacherView();
                courseAssignTeacherView.TeacherId = Convert.ToInt32(Reader["TeacherId"].ToString());
                courseAssignTeacherView.TeacherName = Reader["TeacherName"].ToString();
                courseAssignTeacherView.SemesterName = Reader["SemesterName"].ToString();
                courseAssignTeacherView.TeacherName = Reader["TeacherName"].ToString();
                courseAssignTeacherView.CourseId = Convert.ToInt32(Reader["CourseId"].ToString());
                courseAssignTeacherView.CourseCredit = Convert.ToDouble(Reader["CourseCredit"]);
                courseAssignTeacherViews.Add(courseAssignTeacherView);
            }
            Connection.Close();
            return courseAssignTeacherViews;

        }

        //start  for unassign the CourseAssignTeacher Courese
        public List<CourseAssignTeacher> UnassignTeacherAssignCourses()
        {
            List<CourseAssignTeacher> courseAssignTeachers = new List<CourseAssignTeacher>();
            string query = "SELECT * FROM CourseAssignTeacher WHERE IsAssign=0";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                CourseAssignTeacher courseAssignTeacher = new CourseAssignTeacher();
                courseAssignTeacher.Id = Convert.ToInt32(Reader["Id"]);
                courseAssignTeachers.Add(courseAssignTeacher);
            }
            Connection.Close();
            return courseAssignTeachers;
        }
        public int UnassignRequest(CourseAssignTeacher courseAssignTeacher)
        {
            string query = "UPDATE CourseAssignTeacher set IsAssign= " + courseAssignTeacher.IsAssign + " WHERE Id=" + courseAssignTeacher.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //start  for unassign the Enroll Courese
    }
}