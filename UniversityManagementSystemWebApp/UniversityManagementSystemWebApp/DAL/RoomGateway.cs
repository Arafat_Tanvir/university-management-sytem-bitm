﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.DAL
{
    public class RoomGateway:BaseGateway
    {
        public List<Room> ShowAllRooms()
        {
            List<Room> rooms = new List<Room>();
            string query = "SELECT * FROM Room ORDER BY Id DESC;";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {   
                Room room = new Room();
                room.Id = Convert.ToInt32(Reader["Id"]);
                room.RoomNumber = Reader["RoomNumber"].ToString();
                rooms.Add(room);
            }
            Connection.Close();
            return rooms;

        }
    }
}