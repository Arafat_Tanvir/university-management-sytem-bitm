﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class StudentGateway:BaseGateway
    {

        public int Save(Student student)
        {
            string query = "INSERT INTO Student (Name,Email,ContactNumber,Date,Address,RegistrationNumber,DepartmentId) VALUES('" + student.Name + "','" + student.Email + "','" + student.ContactNumber + "','" + student.Date + "','" + student.Address + "','" + student.RegistrationNumber + "','" + student.DepartmentId + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public bool IsStudentEmailExists(string email)
        {
            string query = "SELECT * FROM Student WHERE Email= '" + email + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isStudentEmail = Reader.HasRows;
            Connection.Close();
            return isStudentEmail;
        }

        public List<Student> CheckYearExits (int id)
        {
            List<Student> students=new List<Student>();
            string query = "SELECT * FROM Student WHERE DepartmentId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Student student = new Student();
                student.Id = Convert.ToInt32(Reader["Id"]);
                student.RegistrationNumber = Reader["RegistrationNumber"].ToString();
                students.Add(student);
            }
            Connection.Close();
            return students;
        }

        public List<Student> ShowAllStudents()
        {
            List<Student> students = new List<Student>();
            string query = "SELECT * FROM Student ORDER BY Id DESC;";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Student student = new Student();
                student.Id = Convert.ToInt32(Reader["Id"]);
                student.RegistrationNumber = Reader["RegistrationNumber"].ToString();
                students.Add(student);
            }
            Connection.Close();
            return students;

        }

        public StudentView GetStudentViewDetails(int id)    
        {
            string query = "SELECT * FROM StudentView WHERE StudentId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            StudentView studentView = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                studentView = new StudentView();
                studentView.StudentId = Convert.ToInt32(Reader["StudentId"]);
                studentView.StudentName = Reader["StudentName"].ToString();
                studentView.StudentEmail = Reader["StudentEmail"].ToString();
                studentView.DepartmentId = Convert.ToInt32(Reader["DepartmentId"]);
                studentView.DepartmentName = Reader["DepartmentName"].ToString();
                
            }
            Connection.Close();
            return studentView;
        }
        public StudentView ShowStudentRegistrationInfo(string email) 
        {
            string query = "SELECT * FROM StudentView WHERE StudentEmail='"+email+"'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            StudentView studentView = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                studentView = new StudentView();
                studentView.StudentId = Convert.ToInt32(Reader["StudentId"]);
                studentView.StudentName = Reader["StudentName"].ToString();
                studentView.StudentEmail = Reader["StudentEmail"].ToString();
                studentView.StudentRegistrationNumber = Reader["StudentRegistrationNumber"].ToString();
                studentView.StudentContactNumber = Convert.ToInt32(Reader["StudentContactNumber"]);
                studentView.StudentDate = Convert.ToDateTime(Reader["StudentDate"]);
                studentView.StudentAddress = Reader["StudentAddress"].ToString();
                studentView.DepartmentName = Reader["DepartmentName"].ToString();
            }
            Connection.Close();
            return studentView;
        }
    }
}