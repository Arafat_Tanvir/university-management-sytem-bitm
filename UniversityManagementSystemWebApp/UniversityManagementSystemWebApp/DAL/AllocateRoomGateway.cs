﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class AllocateRoomGateway : BaseGateway
    {

        public int Save(AllocateRoom allocateRoom)
        {
            string query = "INSERT INTO AllocateRoom (DepartmentId,CourseId,RoomId,Day,FromStart,ToEnd,IsAllocate) VALUES('" +
                           allocateRoom.DepartmentId + "','" + allocateRoom.CourseId + "','" + allocateRoom.RoomId +
                           "','" + allocateRoom.Day + "','" + allocateRoom.FromStart + "','" + allocateRoom.ToEnd + "','" + allocateRoom.IsAllocate + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public List<AllocateRoom> CheckAllocateRooms(int roomId, string day)  
        {
            List<AllocateRoom> allocateRooms = new List<AllocateRoom>();
            string query = "SELECT * FROM AllocateRoom WHERE RoomId=" + roomId + "AND Day='" + day + "'" + "AND IsAllocate='" + 0 + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                AllocateRoom allocateRoom = new AllocateRoom();
                allocateRoom.Id = Convert.ToInt32(Reader["Id"].ToString());
                allocateRoom.FromStart = Reader["FromStart"].ToString();
                allocateRoom.ToEnd = Reader["ToEnd"].ToString();
                allocateRooms.Add(allocateRoom);
            }
            Connection.Close();
            return allocateRooms;
        }

        public List<CourseScheduleView> CourseScheduleView(int id)  
        {
            List<CourseScheduleView> courseScheduleViews = new List<CourseScheduleView>();
            string query = "SELECT * FROM CourseScheduleView WHERE CourseId=" + id + "AND IsAllocate="+0 ;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                CourseScheduleView courseScheduleView = new CourseScheduleView();
                courseScheduleView.AllocateRoomId = Convert.ToInt32(Reader["AllocateRoomId"]);
                courseScheduleView.CourseId = Convert.ToInt32(Reader["CourseId"]);
                courseScheduleView.RoomId = Convert.ToInt32(Reader["RoomId"]);
                courseScheduleView.RoomNumber = Reader["RoomNumber"].ToString();
                courseScheduleView.Day = Reader["Day"].ToString();
                courseScheduleView.FromStart = Reader["FromStart"].ToString();
                courseScheduleView.ToEnd = Reader["ToEnd"].ToString();
                courseScheduleViews.Add(courseScheduleView);
            }
            Connection.Close();
            return courseScheduleViews;

        }
        //start  for unassign the Enroll Courese
        public List<AllocateRoom> UnallocateAllRoom()
        {
            List<AllocateRoom> allocateRooms = new List<AllocateRoom>();
            string query = "SELECT * FROM AllocateRoom WHERE IsAllocate=0";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                AllocateRoom allocateRoom = new AllocateRoom();
                allocateRoom.Id = Convert.ToInt32(Reader["Id"]);
                allocateRooms.Add(allocateRoom);
            }
            Connection.Close();
            return allocateRooms;                           
        }
        public int UnallocateRequest(AllocateRoom allocateRoom)
        {
            string query = "UPDATE AllocateRoom set IsAllocate= " + allocateRoom.IsAllocate + " WHERE Id=" + allocateRoom.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //start  for unassign the Enroll Courese
    }
}