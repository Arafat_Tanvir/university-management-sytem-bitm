﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class TeacherGateway:BaseGateway
    {
        //===========SAVE TEACHER==============
        public int Save(Teacher teacher)
        {
            string query = "INSERT INTO Teacher (Name,Address,Email,ContactNumber,DesignationId,DepartmentId,CreditTaken) VALUES('" + teacher.Name + "','" + teacher.Address + "','" + teacher.Email + "','" + teacher.ContactNumber + "','" + teacher.DesignationId + "','" + teacher.DepartmentId + "','" + teacher.CreditTaken + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //===========CHECKED TEACHER EMAIL IS EXISTS==============
        public bool IsTeacherEmailExists(string email)
        {
            string query = "SELECT * FROM Teacher WHERE Email= '" + email + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isTeacherEmail = Reader.HasRows;
            Connection.Close();
            return isTeacherEmail;
        }
        //===========Get Teachar By Department Id==============
        public List<Teacher> GetAllTeachers(int id)
        {
            List<Teacher> teachers = new List<Teacher>();
            string query = "SELECT * FROM Teacher WHERE DepartmentId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Teacher teacher = new Teacher();
                teacher.Id = Convert.ToInt32(Reader["Id"]);
                teacher.Name = Reader["Name"].ToString();
                teachers.Add(teacher);
            }
            Connection.Close();
            return teachers;

        }
        //=============LoadTeacherDetailsByTeacherId===================
        public Teacher GetTeachersInfo(int id)
        {
            string query = "SELECT * FROM Teacher WHERE Id=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Teacher teacher = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                teacher = new Teacher();
                teacher.Id = Convert.ToInt32(Reader["Id"]);
                teacher.Name = Reader["Name"].ToString();
                teacher.Address = Reader["Address"].ToString();
                teacher.Email = Reader["Email"].ToString();
                teacher.ContactNumber = Convert.ToInt32(Reader["ContactNumber"]);
                teacher.CreditTaken = Convert.ToDouble(Reader["CreditTaken"]);
                teacher.DesignationId = Convert.ToInt32(Reader["DesignationId"]);
                teacher.DepartmentId = Convert.ToInt32(Reader["DepartmentId"]);
            }
            Connection.Close();
            return teacher ;
        }

    }
}