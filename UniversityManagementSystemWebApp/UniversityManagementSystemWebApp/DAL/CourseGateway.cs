﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.DAL
{
    public class CourseGateway:BaseGateway
    {
        public int Save(Course course)
        {
            string query = "INSERT INTO Course (Code,Name,Credit,Description,DepartmentId,SemesterId) VALUES('" + course.Code + "','" + course.Name + "','" + course.Credit + "','" + course.Description + "','" + course.DepartmentId + "','" + course.SemesterId + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public bool IsCourseCodeExists(string code)
        {
            string query = "SELECT * FROM Course WHERE Code='" + code + "' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isCourseCode = Reader.HasRows; 
            Connection.Close();
            return isCourseCode;
        }

        public bool IsCourseNameExists(string name)
        {
            string query = "SELECT * FROM Course WHERE Name='" + name + "' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isCourseName = Reader.HasRows;
            Connection.Close();
            return isCourseName;
        }
        //=============LoadCourseByDepartmentId===================
        public List<Course> GetAllCourses(int id)
        {
            List<Course> courses = new List<Course>();
            string query = "SELECT * FROM Course WHERE DepartmentId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Course course = new Course();
                course.Id = Convert.ToInt32(Reader["Id"]);
                course.Code = Reader["Code"].ToString();
                course.Name = Reader["Name"].ToString();
                courses.Add(course);
            }
            Connection.Close();
            return courses;

        }
        public List<CourseSemesterView> GetAllCoursesAndSemseter(int id)    
        {   
            List<CourseSemesterView> courseSemesterViews = new List<CourseSemesterView>();
            string query = "SELECT * FROM CourseSemesterView WHERE DepartmentId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                CourseSemesterView courseSemesterView = new CourseSemesterView();
                courseSemesterView.CourseId = Convert.ToInt32(Reader["CourseId"]);
                courseSemesterView.CourseCode = Reader["CourseCode"].ToString();
                courseSemesterView.CourseName = Reader["CourseName"].ToString();
                courseSemesterView.SemesterName = Reader["SemesterName"].ToString();
                courseSemesterViews.Add(courseSemesterView);
            }
            Connection.Close();
            return courseSemesterViews;

        }
        //=============LoadCourseDetailsByCourseId===================
        public Course GetCoursesInfo(int id)
        {
            string query = "SELECT * FROM Course WHERE Id=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Course course = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                course = new Course();
                course.Id = Convert.ToInt32(Reader["Id"]);
                course.Code = Reader["Code"].ToString();
                course.Name = Reader["Name"].ToString();
                course.Description = Reader["Description"].ToString();
                course.Credit = Convert.ToInt32(Reader["Credit"]);
                course.DepartmentId = Convert.ToInt32(Reader["DepartmentId"]);
                course.SemesterId = Convert.ToInt32(Reader["SemesterId"]);
            }
            Connection.Close();
            return course;
        }
    }
}