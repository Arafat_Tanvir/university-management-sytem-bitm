﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class StudentController : Controller
    {
        [HttpGet]
        public ActionResult Save()  
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            return View();
        }

        [HttpPost]
        public ActionResult Save(Student student)
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            StudentManager studentManager=new StudentManager();

            string email;
            int id=student.DepartmentId;
            departmentManager=new DepartmentManager();
            Department department=departmentManager.GetDepartmentInfo(id);
            string code=department.Code;

            String underscope = new String('-', 1);
            int checkedvalue;
            int i = 001;
            string role = i.ToString().PadLeft(3, '0');
            string year = student.Date.Year.ToString();
            string registrationNumber;
            string message; 

            List<Student> students =studentManager.CheckYearExits(id);
            string digit = "";
            foreach (Student valueStudent in students)
            {
                string firsteight = valueStudent.RegistrationNumber.Substring(valueStudent.RegistrationNumber.Length - 8);
                string str = firsteight.Substring(0, 4);
                if (str==year)
                {
                    string registraion = valueStudent.RegistrationNumber;
                    digit = registraion.Substring(registraion.Length - 3);
                }
            }
            if (students.Count>0)
            {
                if (digit=="")
                {
                    registrationNumber = (code + underscope + year + underscope + role);
                    student.RegistrationNumber = registrationNumber;
                    checkedvalue = studentManager.Save(student);
                    if (checkedvalue == 1)
                    {
                        TempData["Success"] = "Student Save Successfully";
                        email = student.Email;
                        ViewBag.Students = studentManager.ShowStudentRegistrationInfo(email);
                    }
                    if (checkedvalue == 2)
                    {
                        TempData["Failed"] = "Save Failed";
                    }
                    if (checkedvalue == 3)
                    {
                        TempData["Error"] = "Student Email Allready Exits";
                    }
                    return View();
                }
                int a = Convert.ToInt32(digit);
                a += 1;
                string reg = a.ToString().PadLeft(3, '0');
                registrationNumber = (code + underscope + year + underscope + reg);
                student.RegistrationNumber = registrationNumber;
                checkedvalue = studentManager.Save(student);
                if (checkedvalue == 1)
                {
                    TempData["Success"] = "Student Save Successfully";
                    email = student.Email;
                    ViewBag.Students = studentManager.ShowStudentRegistrationInfo(email);
                }
                if (checkedvalue == 2)
                {
                    TempData["Failed"] = "Save Failed";
                }
                if (checkedvalue == 3)
                {
                    TempData["Error"] = "Student Email Allready Exits";
                }
                
                return View();
            }
            else
            {
                registrationNumber = (code + underscope + year + underscope + role);
                student.RegistrationNumber = registrationNumber;
                checkedvalue = studentManager.Save(student);
                if (checkedvalue == 1)
                {
                    TempData["Success"] = "Student Save Successfully";
                    email = student.Email;
                    ViewBag.Students = studentManager.ShowStudentRegistrationInfo(email);
                }
                if (checkedvalue == 2)
                {
                    TempData["Failed"] = "Save Failed";
                }
                if (checkedvalue == 3)
                {
                    TempData["Error"] = "Student Email Allready Exits";
                }
                return View();
            }
        }


	}
}