﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{

    public class TeacherController : Controller
    {
        private DesignationManager designationManager;
        private DepartmentManager departmentManager;
        private TeacherManager teacherManager;
        public TeacherController()
        {
            designationManager = new DesignationManager();
            departmentManager = new DepartmentManager();
            teacherManager = new TeacherManager();
        }

        public ActionResult Save()
        {
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            ViewBag.Designations = designationManager.GetAllDesignations();
            return View();
        }
        [HttpPost]
        public ActionResult Save(Teacher teacher)
        {
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            ViewBag.Designations = designationManager.GetAllDesignations();
            int checkedvalue = teacherManager.Save(teacher);
            if (checkedvalue == 1)
            {
                TempData["Success"] = "Teacher Save Successfully";
            }
            if (checkedvalue == 2)
            {
                TempData["Failed"] = "Save Failed";
            }
            if (checkedvalue == 3)
            {
                TempData["Error"] = "Teacher Email Allready Exits";
            }
            return View();
        }

        
    }
}