﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;
using WebGrease;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class AllocateRoomController : Controller
    {
       [HttpGet]
        public ActionResult Save()  
        {

           DepartmentManager departmentManager = new DepartmentManager();
           ViewBag.Departments = departmentManager.ShowAllDepartments();
           RoomManager roomManager=new RoomManager();
           ViewBag.Rooms=roomManager.ShowAllRooms();
            return View();
        }

        [HttpPost]
        public ActionResult Save(AllocateRoom allocateRoom)
        {
            AllocateRoomManager allocateRoomManager = new AllocateRoomManager();

            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            RoomManager roomManager = new RoomManager();
            ViewBag.Rooms = roomManager.ShowAllRooms();
            int rmId = allocateRoom.RoomId;
            string day = allocateRoom.Day;

            string startFrom = allocateRoom.FromStart;
            string endFrom = allocateRoom.ToEnd;


            string startHour  = startFrom.Substring(0, 2);
            int new_start_hour = Convert.ToInt32(startHour);
            int newstarthour = new_start_hour*60;
            string startMin = startFrom.Substring(startFrom.Length - 2);
            int new_start_minute = Convert.ToInt32(startMin);

            string endHour = endFrom.Substring(0, 2);
            int new_end_hours = Convert.ToInt32(endHour);
            int newEndhour = 60*new_end_hours;

            string  endMin= endFrom.Substring(endFrom.Length - 2);
            int new_end_minute = Convert.ToInt32(endMin);


            int newstart = newstarthour + new_start_minute;
            int newend = newEndhour + new_end_minute;

            int checkedValue;

            List<AllocateRoom> allocateRooms = allocateRoomManager.CheckAllocateRooms(rmId, day);
            if (newstart<newend)
            {
                if (allocateRooms.Count > 0)
                {
                    bool overlap = false;
                    foreach (AllocateRoom allocate in allocateRooms)
                    {
                        string beforeStartHour = allocate.FromStart.Substring(0, 2);
                        int startmour = Convert.ToInt32(beforeStartHour);
                        int totalhour = startmour*60;
                        string beforeStartMin = allocate.FromStart.Substring(startFrom.Length - 2);
                        int startmin = Convert.ToInt32(beforeStartMin);
                        int totalmin = startmin;
                        string beforeEndHour = allocate.ToEnd.Substring(0, 2);
                        int totalEndHours = Convert.ToInt32(beforeEndHour);
                        int totalEndHour = totalEndHours*60;
                        string beforeEndMin = allocate.ToEnd.Substring(startFrom.Length - 2);
                        int endmin = Convert.ToInt32(beforeEndMin);
                        int totalendmin = endmin;


                        int start = totalhour + totalmin;
                        int end = totalEndHour + totalendmin;
                        overlap = (newstart < start && start >= newend || end <= newstart && newend > end);
                        if (overlap == false)
                        {
                            break;
                        }
                    }
                    if (overlap == true)
                    {
                        allocateRoom.IsAllocate = 0;
                        checkedValue = allocateRoomManager.Save(allocateRoom);
                        if (checkedValue == 1)
                        {
                            TempData["Success"] = "Room Allocation Save Successfully";
                            return View();
                        }
                        if (checkedValue == 2)
                        {
                            TempData["Failed"] = "Save Failed";
                            return View();
                        }
                        
                    }
                    else
                    {

                        TempData["Error"] = "Class Room is overlapping";
                        return View();

                    }
                }
                else
                {
                    allocateRoom.IsAllocate = 0;
                    checkedValue = allocateRoomManager.Save(allocateRoom);
                    if (checkedValue == 1)
                    {
                        TempData["Success"] = "Room Allocation Save Successfully";
                        return View();
                    }
                    if (checkedValue == 2)
                    {
                        TempData["Failed"] = "Save Failed";
                        return View();
                    }
                    
                }
            }
            TempData["Error"] = "Please Select startTime<EndTime";
            return View();
            
                
        }         

        
         



        public ActionResult Show()
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            return View();
        }


    

        public JsonResult LoadCourses(int departmentId) 
        {

            CourseManager courseManager = new CourseManager();
            var fulList = courseManager.GetAllCourses(departmentId);
            return Json(fulList);   
        }

        public ActionResult Schedules(int courseId)         
        {

            AllocateRoomManager allocateRoomManager = new AllocateRoomManager();
            List<CourseScheduleView> schedules = allocateRoomManager.CourseScheduleView(courseId);

            string cell="";
            List<string> total=new List<string>();
            if (schedules.Count > 0)
            {
                foreach (CourseScheduleView schedule in schedules)
                {
                    string a=schedule.FromStart.Substring(0, 2);
                    int selectFromAp = Convert.ToInt32(a);
                    string b = schedule.ToEnd.Substring(0, 2);
                    int selectToAp = Convert.ToInt32(b);    
                    cell += ("R.NO:" + schedule.RoomNumber + "," + schedule.Day.Substring(0, 3) + "," +
                             (selectFromAp >= 12
                                 ? schedule.FromStart + "PM"
                                 : schedule.FromStart + "AM") + '-' +
                             (selectToAp >= 12
                                 ? schedule.ToEnd + "PM"
                                 : schedule.ToEnd + "AM ,"));

                }
                total.Add(cell + ";");
            }
            else
            {
                total.Add("Not Scheduled Yet");
            }
            
            return Json(total, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UnallocateAllClassrooms()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UnallocateAllClassrooms(int unsign)  
        {   
            AllocateRoomManager allocateRoomManager=new AllocateRoomManager();
            List<AllocateRoom> allocateRooms = allocateRoomManager.UnallocateAllRoom();

            foreach (AllocateRoom allocate in allocateRooms)
            {
                AllocateRoom allocateRoom = new AllocateRoom();
                allocateRoom.Id = allocate.Id;
                allocateRoom.IsAllocate = unsign;
                allocateRoomManager.UnallocateRequest(allocateRoom);
            }
            TempData["Success"] = "UnallocateAllClassrooms Successfully";
            return View();
        } 
        
	}
}