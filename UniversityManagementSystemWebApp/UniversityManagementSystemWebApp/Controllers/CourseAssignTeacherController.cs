﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class CourseAssignTeacherController : Controller
    {


        //==============View==================
        [HttpGet]
        public ActionResult Save()
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments=departmentManager.ShowAllDepartments();
            return View();
        }
        //==============Save==================
        [HttpPost]
        public ActionResult Save(CourseAssignTeacher courseAssignTeacher)
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            CourseAssignTeacherManager courseAssignTeacherManager = new CourseAssignTeacherManager();
            int courseId=courseAssignTeacher.CourseId;
            if (courseAssignTeacherManager.CourseIsAssignByAnotherTeacher(courseId) == null)
            {
                courseAssignTeacher.IsAssign = 0;
                int checkedvalue = courseAssignTeacherManager.Save(courseAssignTeacher);
                if (checkedvalue == 1)
                {
                    TempData["Success"] = "Course Assign To Teacher Successfully";
                }
                if (checkedvalue == 2)
                {
                    TempData["Failed"] = "Course Assign To Teacher Failed";
                }
                return View();
            }
            TempData["Error"] = "This Course Assign Before";
            return View();

        }

        //==============LoadAllTeacherByDepartmentId==================
        public JsonResult LoadAllTeacherByDepartmentId(int departmentId)    
        {   
            TeacherManager teacherManager=new TeacherManager();
            var teachers = teacherManager.GetAllTeachers(departmentId);
            return Json(teachers);
        }
        //=============LoadAllCourseByDepartmentId===================
        public JsonResult LoadAllCourseByDepartmentId(int departmentId)      
        {   
          
            CourseManager courseManager = new CourseManager();
            var courses = courseManager.GetAllCourses(departmentId);
            return Json(courses);
        }
        //=============LoadTeacherDetailsByTeacherId===================
        public JsonResult LoadTeacherDetailsByTeacherId(int teacherId)  
        {   
            TeacherManager teacherManager = new TeacherManager();
            var teacherDetils = teacherManager.GetTeachersInfo(teacherId);
            return Json(teacherDetils);
        }
        //=============LoadCourseDetailsByCourseId===================
        public JsonResult LoadCourseDetailsByCourseId(int courseId)
        {   
            CourseManager courseManager = new CourseManager();
            var courseDetils = courseManager.GetCoursesInfo(courseId);
            return Json(courseDetils);
        }

        //==============LoadCourseAssignTeacherByTeacherId==================
        public ActionResult LoadCourseAssignTeacherByTeacherId(int teacherId)
        {   
            CourseAssignTeacherManager courseAssignTeacherManager = new CourseAssignTeacherManager();
            var schedules = courseAssignTeacherManager.CheckTeacherCredit(teacherId);
            return Json(schedules, JsonRequestBehavior.AllowGet);
        }


        //=====================Only For Show Data=================
        [HttpGet]
        public ActionResult Show()
        {
            DepartmentManager departmentManager = new DepartmentManager();
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            return View();
        }
        public JsonResult LoadAllCourseAndSemesterByDepartmentId(int departmentId)
        { 
            CourseManager courseManager = new CourseManager();
            var courses = courseManager.GetAllCoursesAndSemseter(departmentId);
            return Json(courses);
        }
        //==============LoadCourseDetailsByDepartmentId==================
        public JsonResult LoadCourseAssignTeacherByCourseId(int courseId)
        {
            var assignTeacher="";
            CourseAssignTeacherManager courseAssignTeacherManager = new CourseAssignTeacherManager();
            var courseStatics = courseAssignTeacherManager.CourseStaticses(courseId);
            if (courseStatics != null)
            {
                assignTeacher = courseStatics.TeacherName;
                return Json(assignTeacher, JsonRequestBehavior.AllowGet);
            }
            assignTeacher = "Not Assigned Yet";
            return Json(assignTeacher, JsonRequestBehavior.AllowGet);
        }
       
	}
}