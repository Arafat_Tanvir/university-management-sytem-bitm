﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class DepartmentController : Controller
    {
        private DepartmentManager departmentManager;
        
        public DepartmentController()
        {
            
            departmentManager=new DepartmentManager();
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

       [HttpPost]
        public ActionResult Save(Department department)
        {
            int checkedvalue= departmentManager.Save(department);
            if (checkedvalue==1)
            {
                TempData["Success"] = "Department Save Successfully";
            }
            if (checkedvalue == 2)
            {
                TempData["Failed"] = "Save Failed";
            }
            if (checkedvalue == 3)
            {
                TempData["Error"] = "Departemnt Code And Name Allready Exits";
            }
            if (checkedvalue == 4)
            {
                TempData["Error"] = "Departemnt Code Allready Exits";
            }
            if (checkedvalue == 5)
            {
                TempData["Error"] = "Departemnt Name Allready Exits";
            }
            return View();
        }

        public ActionResult Show()
        {
            List<Department> departments =departmentManager.ShowAllDepartments();
            return View(departments);
        }
	}
}