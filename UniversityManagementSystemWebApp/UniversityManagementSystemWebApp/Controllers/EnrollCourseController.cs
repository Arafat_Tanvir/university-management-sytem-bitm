﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class EnrollCourseController : Controller
    {
        private StudentManager studentManager;
        private EnrollCourseManager enrollCourseManager;
        public EnrollCourseController()
        {
             studentManager=new StudentManager();
            enrollCourseManager=new EnrollCourseManager();
            
        }

        [HttpGet]
        public ActionResult Save()
        {
            ViewBag.Students = studentManager.ShowAllStudents();
            return View();
        }
        [HttpPost]
        public ActionResult Save(EnrollCourse enrollCourse)
        {
            int studentId = enrollCourse.StudentId;
            int courseId = enrollCourse.CourseId;
            if (enrollCourseManager.CheckEnrollCourses(studentId, courseId) == null)
            {
                ViewBag.Students = studentManager.ShowAllStudents();
                enrollCourse.IsEnroll = 0;
                int checkedvalue = enrollCourseManager.Save(enrollCourse);
                if (checkedvalue == 1)
                {
                    TempData["Success"] = "Department Save Successfully";
                }
                if (checkedvalue == 2)
                {
                    TempData["Failed"] = "Save Failed";
                }
                return View();
            }
            ViewBag.Students = studentManager.ShowAllStudents();
            TempData["Error"] = "This Subject is Already Taken By You";
            return View();

            
            
        }

        public JsonResult LoadStudent(int studentId) 
        {
            StudentManager studentManager = new StudentManager();
            var students = studentManager.GetStudentViewDetails(studentId);
            return Json(students);
        }

        public JsonResult loadCourses(int departmentId)
        {

            CourseManager courseManager = new CourseManager();
            var courses = courseManager.GetAllCourses(departmentId);
            return Json(courses);
        }
	}
}