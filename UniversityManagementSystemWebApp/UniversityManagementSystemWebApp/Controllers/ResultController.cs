﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{
 
    public class ResultController : Controller
    {
       
       

        [HttpGet]
        public ActionResult Save()  
        {
            StudentManager studentManager=new StudentManager();
            ViewBag.Students = studentManager.ShowAllStudents();
            return View();
        }

        public ActionResult Save(Result result)
        {
            ResultManager resultManager=new ResultManager();
            StudentManager studentManager = new StudentManager();

            string grade = result.Grade;
            int studentId = result.StudentId;
            int courseId = result.CourseId;
            if (resultManager.CheckCourseResult(studentId, courseId)!= null)
            {
                ViewBag.Students = studentManager.ShowAllStudents();
                Result results = resultManager.CheckCourseResult(studentId, courseId);
                int id=results.Id;
                Result resultById=new Result();
                resultById.Id = id;
                resultById.Grade = grade;
                int checkValue = resultManager.UpdateCourseResult(resultById);
                if (checkValue == 1)
                {
                    TempData["Success"] = "Grade Update Successfully";
                }
                if (checkValue == 2)
                {
                    TempData["Failed"] = "Grade Update Failed";
                }
                return View();
            }
            else
            {
                ViewBag.Students = studentManager.ShowAllStudents();
                int checkValue = resultManager.Save(result);
                if (checkValue == 1)
                {
                    TempData["Success"] = "Result Save Successfully";
                }
                if (checkValue == 2)
                {
                    TempData["Failed"] = "Save Failed";
                }
                return View();
            }
            
           
        }

        public ActionResult Show()
        {
            StudentManager studentManager=new StudentManager();
            ViewBag.Students = studentManager.ShowAllStudents();
            return View();
        }

        public JsonResult LoadCoursesResult(int studentId,int courseId)
        {
            string result = "";
            ResultManager resultManager=new ResultManager();
            var courseResult = resultManager.CheckCourseResult(studentId, courseId);

            if (courseResult != null)
            {
                result = courseResult.Grade;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            result = "Not Graded Yet";
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        public JsonResult LoadStudent(int studentId)
        {
            StudentManager studentManager = new StudentManager();
            var students = studentManager.GetStudentViewDetails(studentId);
            return Json(students);
        }
        public JsonResult LoadCourses(int studentId)        
        {
            EnrollCourseManager enrollCourseManager = new EnrollCourseManager();
            var courses = enrollCourseManager.GetEnrollCourseViews(studentId);
            return Json(courses);
        }
	}
}