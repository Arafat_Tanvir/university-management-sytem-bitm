﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemWebApp.BLL.Manager;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.Controllers
{
    public class CourseController : Controller
    {
        private DepartmentManager departmentManager;
        private SemesterManager semesterManager;
        private CourseManager courseManager;
        public CourseController()
        {
            departmentManager=new DepartmentManager();
            semesterManager=new SemesterManager();
            courseManager=new CourseManager();
        }
        [HttpGet]
        public ActionResult Save()
        {
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            ViewBag.Semesters = semesterManager.GetAllSemesters();
            return View();
        }

        [HttpPost]
        public ActionResult Save(Course course)
        {
            ViewBag.Departments = departmentManager.ShowAllDepartments();
            ViewBag.Semesters = semesterManager.GetAllSemesters();
            int checkedvalue = courseManager.Save(course);
            if (checkedvalue == 1)
            {
                TempData["Success"] = "Course Save Successfully";
            }
            if (checkedvalue == 2)
            {
                TempData["Failed"] = "Save Failed";
            }
            if (checkedvalue == 3)
            {
                TempData["Error"] = "Course Code And Name Allready Exits";
            }
            if (checkedvalue == 4)
            {
                TempData["Error"] = "Course Code Allready Exits";
            }
            if (checkedvalue == 5)
            {
                TempData["Error"] = "Course Name Allready Exits";
            }
            return View();
        }
        
        [HttpGet]
        public ActionResult UnassignAllCourses()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UnassignAllCourses(int unsign)
        {
            EnrollCourseManager enrollCourseManager=new EnrollCourseManager();
            List<EnrollCourse> enrollCourses =enrollCourseManager.UnassignEnrollCourses();

            CourseAssignTeacherManager courseAssignTeacherManager=new CourseAssignTeacherManager();
            List<CourseAssignTeacher> courseAssignTeacherManagers=courseAssignTeacherManager.UnassignTeacherAssignCourses();

            ResultManager resultManager=new ResultManager();
            List<Result> results = resultManager.UnassignResultCourses();

            
            foreach (CourseAssignTeacher unsigCourseAssignTeacher in courseAssignTeacherManagers)
            {
                CourseAssignTeacher courseAssignTeacher = new CourseAssignTeacher();
                courseAssignTeacher.Id = unsigCourseAssignTeacher.Id;
                courseAssignTeacher.IsAssign = unsign;
                courseAssignTeacherManager.UnassignRequest(courseAssignTeacher);
            }
          
            foreach (EnrollCourse unassignEnrollCourse in enrollCourses)
            {
                EnrollCourse enrollCourse = new EnrollCourse();
                enrollCourse.Id = unassignEnrollCourse.Id;
                enrollCourse.IsEnroll = unsign;
                enrollCourseManager.UnassignRequest(enrollCourse);
            }
            foreach (Result result in results)
            {
                Result resultCourseResult = new Result();
                resultCourseResult.Id = result.Id;
                resultCourseResult.IsResult = unsign;
               resultManager.UnassignRequest(resultCourseResult);
            }
            TempData["Success"] = "Unassign Successfully";
            return View();
        }

    }
}