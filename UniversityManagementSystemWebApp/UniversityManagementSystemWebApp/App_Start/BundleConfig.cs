﻿using System.Web;
using System.Web.Optimization;

namespace UniversityManagementSystemWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bootstrapcss").Include(
                        "~/Public/Assets/bower_components/bootstrap/dist/css/bootstrap.css"));
            bundles.Add(new ScriptBundle("~/bundles/fontawesome").Include(
                        "~/Public/Assets/bower_components/font-awesome/css/font-awesome.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/Ionicons").Include(
                        "~/Public/Assets/bower_components/Ionicons/css/ionicons.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/AdminLTE").Include(
                        "~/Public/Assets/dist/css/AdminLTE.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/dist").Include(
                        "~/Public/Assets/dist/css/skins/_all-skins.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/bower_components").Include(
                        "~/Public/Assets/bower_components/morris.js/morris.css"));
            bundles.Add(new ScriptBundle("~/bundles/jvectormap").Include(
                        "~/Public/Assets/bower_components/jvectormap/jquery-jvectormap.css"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapdatepicker").Include(
                        "~/Public/Assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapdaterangepicker").Include(
                        "~/Public/Assets/bower_components/bootstrap-daterangepicker/daterangepicker.css"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapwysihtml5").Include(
                        "~/Public/Assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Public/Assets/bower_components/jquery/dist/jquery.min.js",
                        "~/Public/Assets/bower_components/bootstrap/dist/js/bootstrap.min.js",
                        "~/Public/Assets/bower_components/raphael/raphael.min.js",
                        "~/Public/Assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js",
                        "~/Public/Assets/bower_components/jquery-knob/dist/jquery.knob.min.js",
                        "~/Public/Assets/bower_components/moment/min/moment.min.js",
                        "~/Public/Assets/bower_components/bootstrap-daterangepicker/daterangepicker.js",
                        "~/Public/Assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        "~/Public/Assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                        "~/Public/Assets/bower_components/fastclick/lib/fastclick.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Public/Assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Public/Assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                      "~/Public/Assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"));

            bundles.Add(new StyleBundle("~/Content/js").Include(
                      "~/Public/Assets/dist/js/adminlte.min.js",
                      "~/Public/Assets/dist/js/pages/dashboard.js",
                      "~/Public/Assets/dist/js/demo.js"));
        }
    }
}
