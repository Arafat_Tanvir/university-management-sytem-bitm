﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UniversityManagementSystemWebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //For Department Route
            routes.MapRoute(
                name: "Department-Save",
                url: "Department/Save",
                defaults: new{controller = "Department",action = "Save"}
            );
            routes.MapRoute(
                name: "Department-Show",
                url: "Department/Show",
                defaults: new{controller = "Department",action = "Show"}
            );
            //For Course Route
            routes.MapRoute(
                name: "Course-Save",
                url: "Course/Save",
                defaults: new { controller = "Course", action = "Save" }
            );

            //For teacher Route
            routes.MapRoute(
                name: "Teacher-Save",
                url: "Teacher/Save",
                defaults: new { controller = "Teacher", action = "Save" }
            );
            //For Course Assign Teacher Route
            routes.MapRoute(
                name: "CourseAssignTeacher-Save",
                url: "CourseAssignTeacher/Save",
                defaults: new { controller = "CourseAssignTeacher", action = "Save" }
            );
            routes.MapRoute(
                name: "CourseAssignTeacher-Show",
                url: "CourseAssignTeacher/Show",
                defaults: new { controller = "CourseAssignTeacher", action = "Show" }
            );
            

            //for Room Allocation Route
            routes.MapRoute(
                name: "AllocateRoom-Save",
                url: "AllocateRoom/Save",
                defaults: new { controller = "AllocateRoom", action = "Save" }
            );
            routes.MapRoute(
                name: "AllocateRoom-Show",
                url: "AllocateRoom/Show",
                defaults: new { controller = "AllocateRoom", action = "Show" }
            );
            //For EnrollCourse Route
            routes.MapRoute(
                name: "EnrollCourse-Save",
                url: "EnrollCourse/Save",
                defaults: new { controller = "EnrollCourse", action = "Save" }
            );
            routes.MapRoute(
                name: "EnrollCoure-Show",
                url: "EnrollCoure/Show",
                defaults: new { controller = "EnrollCoure", action = "Show" }
            );
            //For Student Route
            routes.MapRoute(
                name: "Student-Save",
                url: "Student/Save",
                defaults: new { controller = "Student", action = "Save" }
            );
            //For Result Route
            routes.MapRoute(
                name: "Result-Save",
                url: "Result/Save",
                defaults: new { controller = "Result", action = "Save" }
            );
            routes.MapRoute(
                name: "Result-Show",
                url: "Result/Show",
                defaults: new { controller = "Result", action = "Show" }
            );

            routes.MapRoute(
               name: "Course-UnassignAllCourses",
               url: "Course/UnassignAllCourses",
               defaults: new { controller = "Course", action = "UnassignAllCourses" }
           );

            routes.MapRoute(
               name: "AllocateRoom-UnallocateAllClassrooms",
               url: "AllocateRoom/UnallocateAllClassrooms",
               defaults: new { controller = "AllocateRoom", action = "UnallocateAllClassrooms" }
           );

            //Default Route
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
           

            
        }
    }
}
