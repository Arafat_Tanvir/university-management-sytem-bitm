﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models.ViewModels
{
    public class CourseAssignTeacherView
    {
        public int TeacherId { get; set; }
        public int CourseId { get; set; }
        public double CourseCredit { get; set; }    
        public string TeacherName { get; set; }
        public string SemesterName { get; set; }

    }
}