﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models.ViewModels
{
    public class ResultShowView
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string CourseCode { get; set; }
        public string CourseCredit { get; set; }
        public string CourseName { get; set; }
        public string CourseGrade { get; set; }

    }
}