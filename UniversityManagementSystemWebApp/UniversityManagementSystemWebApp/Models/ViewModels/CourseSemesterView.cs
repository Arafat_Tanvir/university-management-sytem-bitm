﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models.ViewModels
{
    public class CourseSemesterView
    {
        public int CourseId { get; set; }
        public int DepartmentId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode  { get; set; }
        public string SemesterName { get; set; }
    }
}