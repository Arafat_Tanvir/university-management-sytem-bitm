﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models.ViewModels
{
    public class CourseScheduleView
    {
        public int AllocateRoomId { get; set; }
        public int RoomId { get; set; }
        public int CourseId { get; set; }
        public string RoomNumber { get; set; }
        public string Day { get; set; }
        public string FromStart { get; set; }
        public string ToEnd { get; set; }


    }
}