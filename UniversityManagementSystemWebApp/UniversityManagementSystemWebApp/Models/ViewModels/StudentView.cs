﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models.ViewModels
{
    public class StudentView
    {
        public int StudentId { get; set; }
        public string StudentRegistrationNumber { get; set; }       
        public string StudentName { get; set; }
        public string StudentEmail { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int StudentContactNumber { get; set; }
        public DateTime StudentDate { get; set; }
        public string StudentAddress { get; set; }
        

    }
}