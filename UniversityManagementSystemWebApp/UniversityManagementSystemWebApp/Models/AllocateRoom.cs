﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models
{
    public class AllocateRoom
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public int CourseId { get; set; }
        public int RoomId { get; set; }
        public string Day { get; set; }
        public string ToEnd { get; set; }
        public string FromStart { get; set; }
        public int IsAllocate { get; set; }


    }
}