﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models
{
    public class CourseAssignTeacher
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public int TeacherId { get; set; }
        public int CourseId { get; set; }
        public double CreditTaken { get; set; }
        public double ReCredit { get; set; }
        public int IsAssign { get; set; }
        
    }
}