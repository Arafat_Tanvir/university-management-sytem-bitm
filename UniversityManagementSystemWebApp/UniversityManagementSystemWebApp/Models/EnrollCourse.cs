﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemWebApp.Models
{
    public class EnrollCourse
    {
        public int  Id  { get; set; }
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public string Date { get; set; }
        public int IsEnroll { get; set; }

    }
}