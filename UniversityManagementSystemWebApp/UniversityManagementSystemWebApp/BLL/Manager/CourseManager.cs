﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class CourseManager
    {
        private CourseGateway courseGateway;
        public CourseManager()
        {
            courseGateway=new CourseGateway();
        }

        public int Save(Course course)
        {
            int num;
            bool isCourseNameExists = courseGateway.IsCourseNameExists(course.Name);
            bool isCourseCodeExists = courseGateway.IsCourseCodeExists(course.Code);
            if (isCourseNameExists && isCourseCodeExists)
            {
                return num = 3;
            }
            else if (isCourseCodeExists)
            {
                return num = 4;
            }
            else if (isCourseNameExists)
            {
                return num = 5;
            }
            int rowAffected = courseGateway.Save(course);
            if (rowAffected > 0)
            {

                return num = 1;
            }
            return num = 2;
        }
        
        //=============LoadCourseByDepartmentId===================
        public List<Course> GetAllCourses(int id)
        {
            return courseGateway.GetAllCourses(id);
        }
        //=============LoadCourseDetailsByCourseId===================
        public Course GetCoursesInfo(int id)
        {
            return courseGateway.GetCoursesInfo(id);
        }

        public List<CourseSemesterView> GetAllCoursesAndSemseter(int id)
        {
            return courseGateway.GetAllCoursesAndSemseter(id);
        }
    }
}