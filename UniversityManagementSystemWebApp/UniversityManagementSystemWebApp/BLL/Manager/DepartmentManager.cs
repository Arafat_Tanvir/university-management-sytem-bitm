﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class DepartmentManager
    {
        private DepartmentGateway departmentGateway;
        public DepartmentManager()
        {
            departmentGateway = new DepartmentGateway();
        }

        public int Save(Department department)
        {
            int num;    
            bool isDepartmentNameExists = departmentGateway.IsDepartmentNameExists(department.Name);
            bool isDepartmentCodeExists = departmentGateway.IsDepartmentCodeExists(department.Code);
            if (isDepartmentNameExists && isDepartmentCodeExists)
            {
                return num = 3;
            }
            else if (isDepartmentCodeExists)
            {
                return num = 4;
            }
            else if (isDepartmentNameExists)
            {
                return num = 5;
            }
            int rowAffected = departmentGateway.Save(department);
            if (rowAffected>0)
            {

                return num = 1;
            }
            return num = 2;
        }

        public List<Department> ShowAllDepartments()
        {
            return departmentGateway.ShowAllDepartments();
        }

        public Department GetDepartmentInfo(int id)
        {
            return departmentGateway.GetDepartmentInfo(id);
        }
    }
}