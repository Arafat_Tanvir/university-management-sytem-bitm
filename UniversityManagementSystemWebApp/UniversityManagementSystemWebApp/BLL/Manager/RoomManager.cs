﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class RoomManager
    {
        private RoomGateway roomGateway;
        public RoomManager()
        {
            roomGateway=new RoomGateway();
        }

        public List<Room> ShowAllRooms()
        {
            return roomGateway.ShowAllRooms();
        }
    }
}