﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class EnrollCourseManager
    {
        private EnrollCourseGateway enrollCourseGateway;
        public EnrollCourseManager()
        {
            enrollCourseGateway=new EnrollCourseGateway();
        }

        public int Save(EnrollCourse enrollCourse)
        {
            int num;
            int rowAffect = enrollCourseGateway.Save(enrollCourse);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            else
            {
                return num=2;
            }
        }

        public EnrollCourse CheckEnrollCourses(int studentId, int courseId)
        {
            return enrollCourseGateway.CheckEnrollCourses(studentId, courseId);
        }

        public List<EnrollCourseView> GetEnrollCourseViews(int id)
        {
            return enrollCourseGateway.GetEnrollCourseViews(id);
        }
        //start  for unassign the Enroll Courese
        public List<EnrollCourse> UnassignEnrollCourses()
        {
            return enrollCourseGateway.UnassignEnrollCourses();
        }
        public int UnassignRequest(EnrollCourse enrollCourse)
        {
            int num;
            int rowAffect = enrollCourseGateway.UnassignRequest(enrollCourse);
            if (rowAffect > 0)
            {

                return num = 1;
            }
            else
            {
                return num = 2;
            }
        }
        //start  for unassign the Enroll Courese
    }
}