﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class DesignationManager
    {
        private DesignationGateway designationGateway;
        public DesignationManager()
        {
            designationGateway=new DesignationGateway();
        }

        public List<Designation> GetAllDesignations()
        {
            return designationGateway.GetAllDesignations();
        }
    }
}