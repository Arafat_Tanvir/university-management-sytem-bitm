﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class ResultManager
    {
        private ResultGateway resultGateway;
        public ResultManager()
        {
            resultGateway=new ResultGateway();
        }

        public int Save(Result result)
        {
            int num;
            int rowAffect = resultGateway.Save(result);
            if (rowAffect>0)
            {
                return num=1;
            }
            return num=2;

        }

        public Result CheckCourseResult(int studentId, int courseId)
        {
            return resultGateway.CheckCourseResult(studentId, courseId);
        }

        public int UpdateCourseResult(Result result)
        {
            int num;
            int rowAffect= resultGateway.UpdateCourseResult(result);
            if (rowAffect>0)
            {
                return num = 1;
            }
            return num = 2;
        }

        //public List<ResultShowView> SelectedStudentResultViews(int Teacherid,int courseId)
        //{
        //    return resultGateway.SelectedStudentResultViews(Teacherid, courseId);
        //}
        //start  for unassign the IsResult Courese
        public List<Result> UnassignResultCourses()
        {
            return resultGateway.UnassignResultCourses();
        }
        public int UnassignRequest(Result result)
        {
            int num;
            int rowAffect = resultGateway.UnassignRequest(result);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            else
            {
                return num = 2;
            }
        }
        //start  for unassign the IsResult 
    }
}