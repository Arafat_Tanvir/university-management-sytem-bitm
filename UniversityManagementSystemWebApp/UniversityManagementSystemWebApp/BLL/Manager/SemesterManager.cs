﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class SemesterManager
    {
        private SemesterGateway semesterGateway;
        public SemesterManager()
        {
            semesterGateway=new SemesterGateway();
        }

        public List<Semester> GetAllSemesters()
        {
            return semesterGateway.GetAllSemesters();
        }
    }
}