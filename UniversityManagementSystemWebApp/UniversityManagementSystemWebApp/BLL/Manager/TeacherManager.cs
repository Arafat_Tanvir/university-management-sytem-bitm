﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class TeacherManager
    {
        private TeacherGateway teacherGateway;
        public TeacherManager()
        {
            teacherGateway=new TeacherGateway();
        }

        public int Save(Teacher teacher)
        {
            int num;
            bool isTeacherEmailExists = teacherGateway.IsTeacherEmailExists(teacher.Email);
            if (isTeacherEmailExists)
            {
                return num = 3;
            }
            int rowAffect = teacherGateway.Save(teacher);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            else
            {
                return num = 2;
            }
        }
        //==============LoadTeacherByDepartmentId==================
        public List<Teacher> GetAllTeachers(int id)
        {
            return teacherGateway.GetAllTeachers(id);
        }
        //=============LoadTeacherDetailsByTeacherId===================
        public Teacher GetTeachersInfo(int id)
        {
            return teacherGateway.GetTeachersInfo(id);
        }
    }
}