﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class CourseAssignTeacherManager
    {
        private CourseAssignTeacherGateway courseAssignTeacherGateway;
        public CourseAssignTeacherManager()
        {
            courseAssignTeacherGateway=new CourseAssignTeacherGateway();
        }
        //CourseAssignTeacher Save
        public int Save(CourseAssignTeacher courseAssignTeacher)
        {
            int num;
            int rowAffect = courseAssignTeacherGateway.Save(courseAssignTeacher);
            if (rowAffect>0)
            {
                return num=1;
            }
            return num = 2;
        }
        //==============LoadCourseDetailsByDepartmentId==================
        public CourseAssignTeacherView CourseStaticses(int id)
        {
            return courseAssignTeacherGateway.CourseStaticses(id);
        }
        //Course is Take?
        public CourseAssignTeacher CourseIsAssignByAnotherTeacher(int id)
        {
            return courseAssignTeacherGateway.CourseIsAssignByAnotherTeacher(id);
        }
        //==============LoadCourseAssignTeacherByTeacherId==================
        public List<CourseAssignTeacherView> CheckTeacherCredit(int id)
        {
            return courseAssignTeacherGateway.CheckTeacherCredit(id);
        }


        //start  for unassign the CourseAssignTeacher Courese
        public List<CourseAssignTeacher> UnassignTeacherAssignCourses()
        {
            
            return courseAssignTeacherGateway.UnassignTeacherAssignCourses();
        }
        public int UnassignRequest(CourseAssignTeacher courseAssignTeacher)
        {
            int num;
            int rowAffect = courseAssignTeacherGateway.UnassignRequest(courseAssignTeacher);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            else
            {
                return num = 1;
            }
        }
        //start  for unassign the CourseAssignTeacher Courese
    }
}