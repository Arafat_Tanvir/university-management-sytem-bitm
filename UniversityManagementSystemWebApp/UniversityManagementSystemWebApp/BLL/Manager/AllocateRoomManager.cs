﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class AllocateRoomManager
    {
        private AllocateRoomGateway allocateRoomGateway;
        public AllocateRoomManager()
        {
            allocateRoomGateway=new AllocateRoomGateway();
        }

        public int Save(AllocateRoom allocateRoom)
        {
            int num;
            int rowAffect = allocateRoomGateway.Save(allocateRoom);
            if (rowAffect>0)
            {
                return num = 1;
            }
            return num=2;
            
        }

        public List<AllocateRoom> CheckAllocateRooms(int roomId, string day)
        {
            return allocateRoomGateway.CheckAllocateRooms(roomId, day);
        }

        public List<CourseScheduleView> CourseScheduleView(int id)
        {
            return allocateRoomGateway.CourseScheduleView(id);
        }

        //start  for unassign the Enroll Courese
        public List<AllocateRoom> UnallocateAllRoom()
        {
            return allocateRoomGateway.UnallocateAllRoom();
        }
        public int UnallocateRequest(AllocateRoom allocateRoom)
        {
            int num;
            int rowAffect = allocateRoomGateway.UnallocateRequest(allocateRoom);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            
            return num = 2;
           
        }   
        //start  for unassign the Enroll Courese
    }
}