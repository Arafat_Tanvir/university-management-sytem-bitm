﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemWebApp.DAL;
using UniversityManagementSystemWebApp.Models;
using UniversityManagementSystemWebApp.Models.ViewModels;

namespace UniversityManagementSystemWebApp.BLL.Manager
{
    public class StudentManager
    {
        private StudentGateway studentGateway;
        public StudentManager()
        {
            studentGateway=new StudentGateway();
            
        }

        public int Save(Student student)
        {
            int num;
            bool isTeacherEmailExists = studentGateway.IsStudentEmailExists(student.Email);
            if (isTeacherEmailExists)
            {
                return num = 3;
            }
            int rowAffect = studentGateway.Save(student);
            if (rowAffect > 0)
            {
                return num = 1;
            }
            else
            {
                return num = 2;
            }
        }

        public List<Student> CheckYearExits(int id)
        {
            return studentGateway.CheckYearExits(id);
        }

        public List<Student> ShowAllStudents()
        {
            return studentGateway.ShowAllStudents();
        }

        public StudentView GetStudentViewDetails(int id)
        {
            return studentGateway.GetStudentViewDetails(id);
        }

        public StudentView ShowStudentRegistrationInfo(string email)
        {
            return studentGateway.ShowStudentRegistrationInfo(email);
        }
    }
}