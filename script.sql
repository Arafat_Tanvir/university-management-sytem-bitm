USE [master]
GO
/****** Object:  Database [UniversityManagementSystem]    Script Date: 29-Apr-19 5:07:16 AM ******/
CREATE DATABASE [UniversityManagementSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UniversityManagementSystem', FILENAME = N'E:\Bitm Project\UniversityManagementSystem.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UniversityManagementSystem_log', FILENAME = N'E:\Bitm Project\UniversityManagementSystem_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UniversityManagementSystem] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UniversityManagementSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UniversityManagementSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UniversityManagementSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UniversityManagementSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UniversityManagementSystem] SET  MULTI_USER 
GO
ALTER DATABASE [UniversityManagementSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UniversityManagementSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UniversityManagementSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UniversityManagementSystem]
GO
/****** Object:  Table [dbo].[AllocateRoom]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AllocateRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[Day] [varchar](50) NOT NULL,
	[FromStart] [varchar](50) NOT NULL,
	[ToEnd] [varchar](50) NOT NULL,
	[IsAllocate] [int] NOT NULL,
 CONSTRAINT [PK_AllocateRoom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Course]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Course](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Credit] [decimal](2, 1) NOT NULL,
	[Description] [varchar](max) NULL,
	[DepartmentId] [int] NOT NULL,
	[SemesterId] [int] NOT NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseAssignTeacher]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseAssignTeacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[TeacherId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[IsAssign] [int] NOT NULL,
 CONSTRAINT [PK_CourseAssignTeacher] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Department]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Designation]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Designation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EnrollCourse]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EnrollCourse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Date] [varchar](50) NOT NULL,
	[IsEnroll] [int] NOT NULL,
 CONSTRAINT [PK_EnrollCourse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Result]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Result](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Grade] [varchar](50) NOT NULL,
	[IsResult] [int] NULL,
 CONSTRAINT [PK_Result] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Room]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Room](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Semester]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Semester](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Semester] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[ContactNumber] [int] NULL,
	[Date] [date] NOT NULL,
	[Address] [varchar](50) NOT NULL,
	[RegistrationNumber] [varchar](50) NOT NULL,
	[DepartmentId] [int] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Teacher]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Teacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Address] [varchar](max) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[ContactNumber] [int] NOT NULL,
	[DesignationId] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[CreditTaken] [int] NOT NULL,
 CONSTRAINT [PK_Teacher] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[AllocateRoomView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AllocateRoomView] AS
SELECT c.Id AS CourseId,c.Code AS CourseCode, ar.DepartmentId AS DepartmentId,c.Name AS CourseName, ar.Day,ar.FromStart,ar.ToEnd,r.RoomNumber AS RoomNumber FROM AllocateRoom AS ar INNER JOIN Course AS c ON c.Id=ar.CourseId INNER JOIN Room AS r ON r.Id=ar.RoomId 
GO
/****** Object:  View [dbo].[CourseAssignTeacherView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CourseAssignTeacherView] AS
Select s.Name AS SemesterName,t.Name AS TeacherName,cat.CourseId,cat.TeacherId,c.Credit AS CourseCredit,cat.IsAssign From CourseAssignTeacher AS cat INNER JOIN Course AS c ON cat.CourseId=c.Id INNER JOIN Semester AS s ON s.Id=c.SemesterId INNER JOIN Teacher AS t ON t.Id =cat.TeacherId

GO
/****** Object:  View [dbo].[CourseScheduleView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CourseScheduleView] AS
Select r.Id AS RoomId,r.RoomNumber, ar.FromStart,ar.ToEnd,ar.Day,ar.Id AS AllocateRoomId,ar.CourseId,ar.IsAllocate FROM AllocateRoom AS ar INNER JOIN Room As r ON ar.RoomId=r.Id


GO
/****** Object:  View [dbo].[CourseSemesterView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CourseSemesterView] AS
SELECT c.Id AS CourseId,c.Code AS CourseCode,c.Name AS CourseName,s.Name AS SemesterName,c.DepartmentId FROM Course AS c INNER JOIN Semester AS s ON s.Id=c.SemesterId

GO
/****** Object:  View [dbo].[EnrollCourseView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[EnrollCourseView] AS
SELECT c.Code AS CourseCode,c.Credit AS CourseCredit, ec.IsEnroll,c.Id AS CourseId,c.Name AS CourseName,ec.StudentId FROM EnrollCourse AS ec INNER JOIN Course AS c ON ec.CourseId=c.Id


GO
/****** Object:  View [dbo].[GetAllTeacherView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[GetAllTeacherView] AS
SELECT t.Id,t.Name ,t.Address,t.Email,t.ContactNumber,t.CreditTaken,design.Id As DesignationId,design.Name AS DesignationName,dept.Id As DepartmentId,dept.Name AS DepartmentName,dept.Code AS DepartmentCode ,c.Name AS CourseName,c.Id AS CourseId,c.Credit AS CourseCredit FROM Teacher AS t INNER JOIN Department AS dept on dept.Id=t.DepartmentId INNER JOIN Designation AS design on design.Id=t.DesignationId INNER JOIN Course AS c on c.DepartmentId=dept.Id




GO
/****** Object:  View [dbo].[ResultShowView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ResultShowView] AS
SELECT r.IsResult, r.Id ,r.StudentId,c.Credit AS CourseCredit,c.Code AS CourseCode,c.Name AS CourseName,r.Grade AS CourseGrade FROM Result AS r INNER JOIN Course AS c ON c.Id=r.CourseId 


GO
/****** Object:  View [dbo].[StudentView]    Script Date: 29-Apr-19 5:07:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[StudentView] AS
SELECT s.Id AS StudentId,s.RegistrationNumber AS StudentRegistrationNumber, s.Name AS StudentName,s.Email AS StudentEmail,s.ContactNumber AS StudentContactNumber,s.Date AS StudentDate,s.Address AS StudentAddress,d.Name AS DepartmentName,d.Id AS DepartmentId FROM Student AS s INNER JOIN Department AS d ON d.Id=s.DepartmentId



GO
SET IDENTITY_INSERT [dbo].[AllocateRoom] ON 

INSERT [dbo].[AllocateRoom] ([Id], [DepartmentId], [CourseId], [RoomId], [Day], [FromStart], [ToEnd], [IsAllocate]) VALUES (2036, 5, 1028, 10, N'Monday', N'08:15', N'09:15', 0)
INSERT [dbo].[AllocateRoom] ([Id], [DepartmentId], [CourseId], [RoomId], [Day], [FromStart], [ToEnd], [IsAllocate]) VALUES (2037, 5, 1027, 9, N'Monday', N'08:15', N'09:15', 0)
INSERT [dbo].[AllocateRoom] ([Id], [DepartmentId], [CourseId], [RoomId], [Day], [FromStart], [ToEnd], [IsAllocate]) VALUES (2038, 5, 1030, 8, N'Monday', N'03:15', N'04:15', 0)
INSERT [dbo].[AllocateRoom] ([Id], [DepartmentId], [CourseId], [RoomId], [Day], [FromStart], [ToEnd], [IsAllocate]) VALUES (2039, 5, 1024, 12, N'Saturday', N'08:10', N'09:10', 0)
SET IDENTITY_INSERT [dbo].[AllocateRoom] OFF
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1023, N'CSE-1103', N'Computer Basic and Programming', CAST(2.0 AS Decimal(2, 1)), N'This is Good Sublect', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1024, N'CSE-1104', N'Computer Basic and Programming Sessional ', CAST(1.0 AS Decimal(2, 1)), N'', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1025, N'EEE-1101', N'Electrical Circuits I', CAST(3.0 AS Decimal(2, 1)), N'WELCOME', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1026, N'EEE-1102', N'Electrical Circuits I Sessional', CAST(1.0 AS Decimal(2, 1)), N'WELCOME', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1027, N'PHY-I', N'Physics I', CAST(3.0 AS Decimal(2, 1)), N'welcome', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1028, N'PHY-1102', N'Physics I Sessional', CAST(1.0 AS Decimal(2, 1)), N'Welcome', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1029, N'MATH-1101', N'Math I ', CAST(3.0 AS Decimal(2, 1)), N'Welcome', 5, 1)
INSERT [dbo].[Course] ([Id], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId]) VALUES (1030, N'STAT-1201', N'Statistics', CAST(2.0 AS Decimal(2, 1)), N'Welcome', 5, 2)
SET IDENTITY_INSERT [dbo].[Course] OFF
SET IDENTITY_INSERT [dbo].[CourseAssignTeacher] ON 

INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3044, 5, 1016, 1023, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3045, 5, 1016, 1024, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3046, 5, 1017, 1025, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3047, 5, 1017, 1026, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3048, 5, 1017, 1029, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3049, 5, 1021, 1028, 0)
INSERT [dbo].[CourseAssignTeacher] ([Id], [DepartmentId], [TeacherId], [CourseId], [IsAssign]) VALUES (3050, 5, 1017, 1027, 0)
SET IDENTITY_INSERT [dbo].[CourseAssignTeacher] OFF
SET IDENTITY_INSERT [dbo].[Department] ON 

INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (1, N'ETE', N'Electronic & Telecommunication Engineering ')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (2, N'EEE', N'Electrical and Electronics Engineering')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (5, N'CSE', N'Computer Science and Engineering')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (11, N'BBA', N'Bachelor of Business Administration')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (21, N'EB', N'Economics with Banking')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (2021, N'ENG', N'English')
INSERT [dbo].[Department] ([Id], [Code], [Name]) VALUES (3036, N'BN', N'Bangla')
SET IDENTITY_INSERT [dbo].[Department] OFF
SET IDENTITY_INSERT [dbo].[Designation] ON 

INSERT [dbo].[Designation] ([Id], [Name]) VALUES (1, N'Professor')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (2, N'Associate Professor')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (3, N'Assistant Professor')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (4, N'Lecturer')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (5, N'Senior Lecturer')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (6, N'Adjunct Assistant Professor')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (7, N'Adjunct Associate Professor')
INSERT [dbo].[Designation] ([Id], [Name]) VALUES (8, N'Adjunct Professor')
SET IDENTITY_INSERT [dbo].[Designation] OFF
SET IDENTITY_INSERT [dbo].[EnrollCourse] ON 

INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1036, 1058, 1029, N'2019-04-28', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1037, 1058, 1025, N'2019-04-28', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1038, 1058, 1024, N'2019-04-29', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1039, 1058, 1023, N'2019-04-29', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1040, 1058, 1026, N'2019-04-29', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1041, 1058, 1030, N'2019-04-29', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1042, 1058, 1027, N'2019-04-29', 0)
INSERT [dbo].[EnrollCourse] ([Id], [StudentId], [CourseId], [Date], [IsEnroll]) VALUES (1043, 1058, 1028, N'2019-04-29', 0)
SET IDENTITY_INSERT [dbo].[EnrollCourse] OFF
SET IDENTITY_INSERT [dbo].[Result] ON 

INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3018, 1058, 1029, N'B-', 0)
INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3019, 1058, 1025, N'A+', 0)
INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3020, 1058, 1026, N'A-', 0)
INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3021, 1058, 1023, N'A+', 0)
INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3022, 1058, 1024, N'B+', 0)
INSERT [dbo].[Result] ([Id], [StudentId], [CourseId], [Grade], [IsResult]) VALUES (3023, 1058, 1030, N'A+', 0)
SET IDENTITY_INSERT [dbo].[Result] OFF
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (1, N'S-501')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (2, N'S-502')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (3, N'S-101')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (4, N'S-102')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (5, N'S-201')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (6, N'S-202')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (7, N'S-301')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (8, N'S-302')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (9, N'S-401')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (10, N'S-402')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (11, N'S-601')
INSERT [dbo].[Room] ([Id], [RoomNumber]) VALUES (12, N'S-602')
SET IDENTITY_INSERT [dbo].[Room] OFF
SET IDENTITY_INSERT [dbo].[Semester] ON 

INSERT [dbo].[Semester] ([Id], [Name]) VALUES (1, N'1st')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (2, N'2nd')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (3, N'3rd')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (4, N'4th')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (5, N'5th')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (6, N'6th')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (7, N'7th')
INSERT [dbo].[Semester] ([Id], [Name]) VALUES (8, N'8th')
SET IDENTITY_INSERT [dbo].[Semester] OFF
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1058, N'Kamrul Hasan', N'karmul@gmail.com', 1827676528, CAST(0x983F0B00 AS Date), N'erwe', N'CSE-2019-001', 5)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1059, N'Shishir Ahasan', N'tanila@gamil.com', 1827676528, CAST(0x993F0B00 AS Date), N'dfgh', N'CSE-2019-002', 5)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1060, N'Shishir Ahasan', N'salimkutubi@gamil.com', 1827676528, CAST(0x993F0B00 AS Date), N'dfs', N'CSE-2019-003', 5)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1061, N'Shishir Ahasan', N'raihan@gmail.com', 1827676528, CAST(0x993F0B00 AS Date), N'dsas', N'BBA-2019-001', 11)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1062, N'Shishir Ahasan', N'Hsif@gmal.com', 1571771367, CAST(0x993F0B00 AS Date), N'dsfsdf', N'BBA-2019-002', 11)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1063, N'Shishir Ahasan', N'shishirahaswan@gmail.com', 1827676528, CAST(0x1C410B00 AS Date), N'dfsf', N'BBA-2020-001', 11)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1064, N'Shishir Ahasan', N'dshsdlf@gmail.com', 1827676528, CAST(0xBB3E0B00 AS Date), N'sfddf', N'BBA-2018-001', 11)
INSERT [dbo].[Student] ([Id], [Name], [Email], [ContactNumber], [Date], [Address], [RegistrationNumber], [DepartmentId]) VALUES (1065, N'Shishir Ahasan', N'welcome@gmail.com', 1827676528, CAST(0x993F0B00 AS Date), N'ksfsd', N'CSE-2019-004', 5)
SET IDENTITY_INSERT [dbo].[Student] OFF
SET IDENTITY_INSERT [dbo].[Teacher] ON 

INSERT [dbo].[Teacher] ([Id], [Name], [Address], [Email], [ContactNumber], [DesignationId], [DepartmentId], [CreditTaken]) VALUES (1016, N'Md.Kamrul Hasan', N'He is Physices Teacher Basically', N'kamrulhasancse15@gmail.com', 0, 2, 5, 15)
INSERT [dbo].[Teacher] ([Id], [Name], [Address], [Email], [ContactNumber], [DesignationId], [DepartmentId], [CreditTaken]) VALUES (1017, N'Aklima Sultana Mishu', N'lemshi-khali', N'aklimasultana@gmail.com', 0, 1, 5, 4)
INSERT [dbo].[Teacher] ([Id], [Name], [Address], [Email], [ContactNumber], [DesignationId], [DepartmentId], [CreditTaken]) VALUES (1021, N'Shishir Ahasan', N'Bangladesh', N'shishirahasan@gmail.com', 1827676528, 2, 5, 12)
SET IDENTITY_INSERT [dbo].[Teacher] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [Code_Course_Unique]    Script Date: 29-Apr-19 5:07:17 AM ******/
ALTER TABLE [dbo].[Course] ADD  CONSTRAINT [Code_Course_Unique] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Name_Course_Unique]    Script Date: 29-Apr-19 5:07:17 AM ******/
ALTER TABLE [dbo].[Course] ADD  CONSTRAINT [Name_Course_Unique] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IsCode_Department_Unique]    Script Date: 29-Apr-19 5:07:17 AM ******/
ALTER TABLE [dbo].[Department] ADD  CONSTRAINT [IsCode_Department_Unique] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IsName_Department_Unique]    Script Date: 29-Apr-19 5:07:17 AM ******/
ALTER TABLE [dbo].[Department] ADD  CONSTRAINT [IsName_Department_Unique] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Email_Teacher_Unique]    Script Date: 29-Apr-19 5:07:17 AM ******/
ALTER TABLE [dbo].[Teacher] ADD  CONSTRAINT [Email_Teacher_Unique] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllocateRoom]  WITH CHECK ADD  CONSTRAINT [FK_AllocateRoom_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[AllocateRoom] CHECK CONSTRAINT [FK_AllocateRoom_Course]
GO
ALTER TABLE [dbo].[AllocateRoom]  WITH CHECK ADD  CONSTRAINT [FK_AllocateRoom_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[AllocateRoom] CHECK CONSTRAINT [FK_AllocateRoom_Department]
GO
ALTER TABLE [dbo].[AllocateRoom]  WITH CHECK ADD  CONSTRAINT [FK_AllocateRoom_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Room] ([Id])
GO
ALTER TABLE [dbo].[AllocateRoom] CHECK CONSTRAINT [FK_AllocateRoom_Room]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Department]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Semester] FOREIGN KEY([SemesterId])
REFERENCES [dbo].[Semester] ([Id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Semester]
GO
ALTER TABLE [dbo].[CourseAssignTeacher]  WITH CHECK ADD  CONSTRAINT [FK_CourseAssignTeacher_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[CourseAssignTeacher] CHECK CONSTRAINT [FK_CourseAssignTeacher_Course]
GO
ALTER TABLE [dbo].[CourseAssignTeacher]  WITH CHECK ADD  CONSTRAINT [FK_CourseAssignTeacher_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[CourseAssignTeacher] CHECK CONSTRAINT [FK_CourseAssignTeacher_Department]
GO
ALTER TABLE [dbo].[CourseAssignTeacher]  WITH CHECK ADD  CONSTRAINT [FK_CourseAssignTeacher_Teacher] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[Teacher] ([Id])
GO
ALTER TABLE [dbo].[CourseAssignTeacher] CHECK CONSTRAINT [FK_CourseAssignTeacher_Teacher]
GO
ALTER TABLE [dbo].[EnrollCourse]  WITH CHECK ADD  CONSTRAINT [FK_EnrollCourse_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[EnrollCourse] CHECK CONSTRAINT [FK_EnrollCourse_Course]
GO
ALTER TABLE [dbo].[EnrollCourse]  WITH CHECK ADD  CONSTRAINT [FK_EnrollCourse_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[EnrollCourse] CHECK CONSTRAINT [FK_EnrollCourse_Student]
GO
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [FK_Result_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [FK_Result_Course]
GO
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [FK_Result_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [FK_Result_Student]
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [FK_Teacher_Department]
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Designation] FOREIGN KEY([DesignationId])
REFERENCES [dbo].[Designation] ([Id])
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [FK_Teacher_Designation]
GO
USE [master]
GO
ALTER DATABASE [UniversityManagementSystem] SET  READ_WRITE 
GO
